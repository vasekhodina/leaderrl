extends Node
class_name Main

var turn_order : Array[Node]

func _ready() -> void:
	turn_order = get_tree().get_nodes_in_group("turn_order")
	turn_process()


func turn_process() -> void:
	var turn_number : int = 0
	var index : int = 0
	while true:
		var current_actor : Character = turn_order[index] as Character
		current_actor.play_turn()
		if current_actor.type == current_actor.PLAYER:
			await current_actor.turn_finished
		index += 1
		if index >= turn_order.size():
			index = 0
			print("Turn {0}".format([turn_number]))
			turn_number += 1


func _on_character_turn_finished() -> void:
	pass # Replace with function body.
	


extends Area2D

class_name Entity

@onready var _tile_map : GameMap = $"../../Map"

@export var passable : bool = true

var map_position : Vector2i
var _tween : Tween = null

signal message(bbcode:String)

func _ready():
	map_position = _tile_map.local_to_map(position)
	self.message.connect((get_node("/root/Main/GUI") as GUI).add_message)

func set_map_position(new_map_position:Vector2) -> void:
	map_position = new_map_position
	set_position(_tile_map.map_to_local(new_map_position))
	

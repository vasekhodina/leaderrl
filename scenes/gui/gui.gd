extends Control
class_name GUI

func add_message(bbcode:String) -> void:
	($VBoxContainer/MessageLog as RichTextLabel).append_text(bbcode)

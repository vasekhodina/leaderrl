extends Entity

class_name Character

enum {PLAYER, NPC}

@export var type : int = NPC
@export var initial_target_path : NodePath = ""
@export var realtime_mode := false

signal turn_finished

var await_input : bool = false
var target : Entity = null
var target_map_position := Vector2i(0,0)
var inventory : Array = []


func _ready() -> void:
	target = get_node_or_null(initial_target_path)
	super._ready()

func _physics_process(_delta):
	if await_input:
		if Input.is_action_just_pressed("step_north"):
			if move(Vector2i(0,-1)):
				end_player_turn()
		if Input.is_action_just_pressed("step_south"):
			if move(Vector2i(0,1)):
				end_player_turn()
		if Input.is_action_just_pressed("step_west"):
			if move(Vector2i(-1,0)):
				end_player_turn()
		if Input.is_action_just_pressed("step_east"): 
			if move(Vector2i(1,0)):
				end_player_turn()
		if Input.is_action_just_pressed("step_northwest"): 
			if move(Vector2i(-1,-1)):
				end_player_turn()
		if Input.is_action_just_pressed("step_northeast"): 
			if move(Vector2i(1,-1)):
				end_player_turn()
		if Input.is_action_just_pressed("step_southwest"): 
			if move(Vector2i(-1,1)):
				end_player_turn()
		if Input.is_action_just_pressed("step_southeast"): 
			if move(Vector2i(1,1)):
				end_player_turn()
		if Input.is_action_just_pressed("ui_accept"):
			end_player_turn()
		if Input.is_action_just_pressed("pick_up_item"):
			pick_up_item(get_overlapping_areas()[0])
			end_player_turn()
		if Input.is_action_just_pressed("show_inventory"):
			show_inventory()

func move(direction:Vector2i) -> bool:
	var new_map_position : Vector2 = map_position + direction
	var raycast_position := to_local(_tile_map.to_global(_tile_map.map_to_local(new_map_position)))
	($RayCast as RayCast2D).set_target_position(raycast_position)
	($RayCast as RayCast2D).force_raycast_update()
	var blocking_entity = ($RayCast as RayCast2D).get_collider()
	if blocking_entity:
		var blocking_character : Character = blocking_entity as Character
		if blocking_character:
			blocking_character.interact()
		else:
			pass
	else:
		set_map_position(new_map_position)
		_tween = create_tween().set_trans(Tween.TRANS_QUART).set_ease(Tween.EASE_IN_OUT)
		_tween.tween_property($Sprite2D, "position", Vector2(0,0), 1).from(10*-1*Vector2(direction))      
		return true
	return false

func npc_turn() -> void:
#	var move_direction : Vector2 = Vector2(randi_range(-1,1),randi_range(-1,1))
	if target:
		if target_map_position != target.map_position:
			target_map_position = target.map_position
		var path_to_target : PackedVector2Array = _tile_map.find_path(map_position, target_map_position)
		print(path_to_target)
		var steps_to_target : PackedVector2Array = []
		for x in range(1,len(path_to_target)):
			steps_to_target.append(path_to_target[x] - path_to_target[x-1])
		print(steps_to_target)
		move(steps_to_target[0])
	#move(move_direction)


func end_player_turn() -> void:
	await_input = false
	await get_tree().physics_frame
	emit_signal("turn_finished")


func play_turn() -> void:
	if type == PLAYER:
		await_input = true
		if realtime_mode:
			($PlayerTurnTimer as Timer).start()
	if type == NPC:
		npc_turn()

func interact() -> void:
	print("%s was interacted with." % [name])


func pick_up_item(item:Item) -> void:
	inventory.append(item.item_data.get("name", "Item Name Missing"))
	item.queue_free()
	emit_signal("message", "%s picks up %s" % [self.name, item.item_name])


func show_inventory() -> void:
	emit_signal("message", "Inventory: %s" % [inventory])


func _on_player_turn_timer_timeout():
	if await_input:
		end_player_turn()


func _on_area_entered(area : Area2D):
	var item : Item = area as Item
	if item:
		emit_signal("message", "There is %s lying here." % item.item_name)

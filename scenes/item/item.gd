extends Entity
class_name Item

var item_data : Dictionary = {}

@export var item_name : String = "Item name missing"
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func load_item_data() -> void:
	(get_node("Sprite2D") as Sprite2D).set_modulate(item_data["color"])

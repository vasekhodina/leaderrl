extends TileMap
class_name GameMap

var _navigation := AStarGrid2D.new()

func _ready() -> void:
	_navigation.region = Rect2i(0,0,32,32)
	_navigation.cell_size = self.tile_set.tile_size
	_navigation.update()
	
	for x in 32:
		for y in 32:
			var position = Vector2i(x, y)
			if get_cell_source_id(0, position) == 0:
				_navigation.set_point_solid(position)

func find_path(start_coords, end_coords) -> PackedVector2Array:
	return _navigation.get_id_path(start_coords, end_coords)

